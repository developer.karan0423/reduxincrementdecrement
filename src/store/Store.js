import { CountReducer } from '../reducer/CountReducer';
import { createStore } from 'redux';

export const STORE = createStore(CountReducer);