export const CountReducer = (state = { count : 0 }, action) => {
    let currentState = { ...state };
    switch (action.type) {
        case 'INCREMENT_COUNTER':
            currentState = { ...currentState, count: currentState.count + 1 };
            break;
        case 'DECREMENT_COUNTER':
            if(currentState.count === 0)
                break;
            else
                currentState = { ...currentState, count: currentState.count -1 };
            break;
        case 'RESET_COUNTER':
            currentState = { ...currentState, count: 0 };
            break;
        default:
            break;
    }
    return currentState;
}