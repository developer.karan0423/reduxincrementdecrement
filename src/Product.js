import React from 'react';
import { connect } from 'react-redux';

function Product(props) {
    return(
        <div className="bg-color">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-3">
                        <div className="productqty text-center">
                            <div className="input-group">
                                <span className="input-group-btn">
                                    <button className="quantity-left-minus btn btn-danger btn-number"
                                        onClick = { () => props.decrement() }
                                    >
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </span>
                                <input type="text" id="quantity" name="quantity" className="form-control input-number" value={`${props.count}`} disabled />
                                <span className="input-group-btn">
                                    <button className="quantity-right-plus btn btn-success btn-number"
                                        onClick = { () => props.increment() }
                                    >
                                    <i class="fas fa-plus"></i>
                                    </button>
                                </span>
                            </div>
                            <button className="btn btn-info mt-3"
                                onClick = { () => props.reset() }
                            >Reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

const MapStateToProps = (state) => {
    return state;
}

const MapDispatchToProps = (dispatch) => {
    return{
        increment: () => dispatch({ type: 'INCREMENT_COUNTER' }),
        decrement: () => dispatch({ type: 'DECREMENT_COUNTER' }),
        reset: () => dispatch({ type: 'RESET_COUNTER' }),
    };
}

export default connect(MapStateToProps,MapDispatchToProps)(Product);